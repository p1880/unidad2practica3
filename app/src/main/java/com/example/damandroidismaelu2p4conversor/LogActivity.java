package com.example.damandroidismaelu2p4conversor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class LogActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "LOG-";

    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onStart " );
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onStop ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onPause ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onResume ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestart ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //true finaliza el usuario / false finalizxa el sistema.
        Log.i(DEBUG_TAG+ getLocalClassName(), " onDestry " + textoisFishing());
    }

    //se llama cuando se destruye la aplicacion, como cuando giras la pantalla
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestore ");
    }
    //sirve para crear un save de la aplicacion.
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onSave ");
    }

    private String textoisFishing(){
        if(isFinishing()){
            return "El usuario a cerrado la aplicacion.";
        }else{
            return "El sistema a cerrado la aplicacion.";
        }
    }
}
