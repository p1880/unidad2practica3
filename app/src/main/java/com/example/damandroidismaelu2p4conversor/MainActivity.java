package com.example.damandroidismaelu2p4conversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends LogActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI(){
        EditText etPulgada = findViewById(R.id.etPulgada);
        EditText etResultado = findViewById(R.id.etResultado);
        Button btConvertirC = findViewById(R.id.btConvertirC);
        Button btConvertirP = findViewById(R.id.btConvertirP);

        btConvertirC.setOnClickListener(view -> {
            try {
                if(Integer.valueOf(String.valueOf(etPulgada.getText())) >= 1) {
                    etResultado.setText(convertirCentimetros(etPulgada.getText().toString()));
                }else{
                    etPulgada.setError("Sólo números >=1");
                    throw new RuntimeException("Sólo números >=1");
                }
            }catch(Exception e){
                Log.e("LogsConversor", e.getMessage());
            }
        });

        btConvertirP.setOnClickListener(view -> {
            try {
                if(Integer.valueOf(String.valueOf(etPulgada.getText())) >= 1){
                    etResultado.setText(convertirPulgadas(etPulgada.getText().toString()));
                }else {
                    etPulgada.setError("Sólo números >=1");
                    throw new RuntimeException("Sólo números >=1");
                }
            }catch(Exception e){
                Log.e("LogsConversor", e.getMessage());
            }
        });
    }

    private String convertirCentimetros(String pulgadaText){
        double pulgadaValue = Math.round((Double.parseDouble(pulgadaText) * 2.54)*100.0)/100.0;

        return String.valueOf(pulgadaValue);
    }

    private String convertirPulgadas(String pulgadaText){
        double pulgadaValue = Math.round((Double.parseDouble(pulgadaText) / 2.54)*100.0)/100.0;
        return String.valueOf(pulgadaValue);
    }
}